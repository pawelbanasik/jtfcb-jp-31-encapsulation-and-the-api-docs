
class Plant {
	
	
    // Instance variables should be declared private, 
    // or at least protected.
	// dane w klasach robimy private
	// w klasie dziecku robimy protected jak juz trzeba
	// raczej robimy prywatne w srodku klasy int i string
	// zeby dostac sie do private data uzywamy geters i seters
	// kilka metod typowych public typu geters i seters
	// reszte staramy sie trzymac w klasie
	private String name;
	
	
    // Usually only static final members are public
	// chyba ze final static
	public static final int ID = 7;
	
	
	
	
	
    // Only methods intended for use outside the class
    // should be public. These methods should be documented
    // carefully if you distribute your code.
	public String getData() {
		
		String data = "some stuff" + calculateGrowthForecast();
		return data;
		
	}

	
	// Methods only used the the class itself should
    // be private or protected.
	private int calculateGrowthForecast() {
		
		return 9;
		
	}
	// source i generujesz geters i seters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}

public class App {

	public static void main(String[] args) {
		

	}

}
